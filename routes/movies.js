var express = require('express');
const _ = require('lodash');
const axios = require('axios');
var router = express.Router();

let movies = [{
    id: "0",
    movie: "Inception",
}];

const omdbKey = "8155560e"

/* GET Movies list */
router.get('/', (req, res, next) => {
    res.status(200).json( { movies } )
});

/* GET one movie */
router.get('/:id', (req, res, next) => {
    const { id } = req.params;
    const movie = _.find(movies, ["id", id]);
    res.status(200).json({
        message: 'Film found!',
        movie 
    });
});

/* PUT a new movie */
router.put('/', (req, res) => {
    const { movie } = req.body;
    const id = _.uniqueId();

    axios.get(`http://www.omdbapi.com/?apiKey=${omdbKey}&t=${movie}`)
        .then((response) => {
            if (response.data.Response) {
                movies.push({
                    movie,
                    id,
                    yearOfRelease: response.data.Released,
                    duration: response.data.Runtime,
                    actors: response.data.Actors,
                    poster: response.data.Poster,
                    boxOffice: response.data.BoxOffice
                });
            }
        });

    res.json({
        message: `Just added ${id}`,
        movie: { movie, id }
    })
});


module.exports = router;